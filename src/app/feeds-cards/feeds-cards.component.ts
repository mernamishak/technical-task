import {Component, OnInit, ViewChild} from '@angular/core';
import {faHeart, faCommentDots} from '@fortawesome/free-solid-svg-icons';
import {FontawesomeObject} from "@fortawesome/fontawesome-svg-core";

@Component({
    selector: 'app-feeds-cards',
    templateUrl: './feeds-cards.component.html',
    styleUrls: ['./feeds-cards.component.scss']
})
export class FeedsCardsComponent implements OnInit {
    cardMap = new Map();
    faHeart = faHeart;
    faCommentDots = faCommentDots;
    likes = 0;
    totalLikes = 0;
    cardID;

    ngOnInit() {
        if (localStorage.myMap) {
            this.cardID = new Map(JSON.parse(localStorage.myMap));
            this.cardID.forEach((value: string, key: string) => {
                let element = document.querySelector('#text_' + key);
                const noOfLikes = document.createTextNode(value);
                element.appendChild(noOfLikes);
                let btn = document.getElementById(key).firstElementChild.classList.add('active-btn')
            });

        }

    }

    favorite(event) {
        let cardNo = event.path[3].nodeName === 'BUTTON' ?
            event.path[3].attributes[1].value : event.path[2].attributes[1].value;
        if (this.cardID) {
            this.likes = this.cardMap.get(cardNo)
        }
        this.likes = this.likes + 1
        this.cardMap.set(cardNo, this.likes.toString());


        document.getElementById(cardNo).firstElementChild.classList.add('active-btn');
        this.totalLikes = this.cardMap.get(cardNo);
        localStorage.myMap = JSON.stringify(Array.from(this.cardMap.entries()));
    }
}