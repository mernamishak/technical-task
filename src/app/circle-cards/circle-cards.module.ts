import {NgModule} from '@angular/core';
import {FontAwesomeModule} from "@fortawesome/angular-fontawesome";
import {circleCardsComponent} from "./circle-cards.component";



@NgModule({
    declarations: [
        circleCardsComponent

    ],
    imports: [
        FontAwesomeModule
    ],
    exports: [
        circleCardsComponent
    ]
})
export class CircleCardsModule {
}