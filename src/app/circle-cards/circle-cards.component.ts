import {Component, OnInit} from '@angular/core';
import {faPlus} from '@fortawesome/free-solid-svg-icons';

@Component({
    selector: 'app-circle-cards',
    templateUrl: './circle-cards.component.html',
    styleUrls: ['./circle-cards.component.scss']
})
export class circleCardsComponent implements OnInit {
    faPlus = faPlus;

    ngOnInit() {
    }
}